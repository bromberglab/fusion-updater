
import argparse
from .config.configure import create_config_template as create_config_template
from .config.configure import import_config as import_config
from .config.configure import validate_config as validate_config
from .config.configure import read_config as read_config
from .config.configure import save_config as save_config

def main():
    '''
    The main function of 'fusion.py'. Here all of the subroutines and 
    their corresponding argument parsers are defined.
    '''

    '''
    Defining the base argument parser that spawns all of the subparsers 
    associated with individual subcommands.
    '''
    argp = argparse.ArgumentParser()
    parsers = argp.add_subparsers(
        dest = "command",
        title = "commands",
        required = True)

    '''
    The following section defines the subparser for the 'configure' 
    command (fusion.py configure **kwargs)
    '''
    parser_config = parsers.add_parser(
        'configure',
        help = 'configure help'
    )
    pcg = parser_config.add_mutually_exclusive_group(required = True)
    pcg.add_argument(
        '-c', '--create_config_template',
        type=argparse.FileType('w'),
        metavar='TEMPLATE_FILE',
    )
    pcg.add_argument(
        '-i', '--import_config_template',
        type=argparse.FileType('r'),
        metavar='TEMPLATE_FILE'
    )
    pcg.add_argument(
        '-v', '--validate_config', action='store_true'
    )
    parser_config.set_defaults(func=configure)
    
    parser_setup = parsers.add_parser(
        'setup',
        help = 'A subroutine that can be invoked to setup the project and '\
            'working directories for use with other fusion.py subroutines.\n'\
            'Sould be invoked after the configuration file has been properly '\
            'defined (see \'fusion.py configure\') as the folder structure '\
            'used in the setup process is defined there. Alternatively the '\
            'basic working directory as well as project directory '\
            '(\'-wd\' & \'-pd\') can be manually defined while invoking '\
            '\'fusion.py setup\' and will then be stored into the exsiting '\
            'config file.'
    )
    parser_setup.add_argument(
        '-wd', '--working_directory',
        dest = 'WORKING_DIRECTORY',
        type = str
    )
    parser_setup.add_argument(
        '-pd', '--project_directory',
        dest = 'PROJECT_DIRECTORY',
        type = str
    )
    parser_setup.set_defaults(func=setup_folders)

    args = argp.parse_args()
    args.func(args)

def configure(args):
    if args.create_config_template:
        create_config_template(args.create_config_template)
    elif args.import_config_template:
        filepath = args.import_config_template.name
        args.import_config_template.close()
        import_config(filepath)
    elif args.validate_config:
        validate_config()

def setup_folders(args):
    config = read_config()
    if args.WORKING_DIRECTORY:
        config['Paths']['workdir'] = args.WORKING_DIRECTORY
    if args.PROJECT_DIRECTORY:
        config['Paths']['projectdir'] = args.PROJECT_DIRECTORY
    save_config(config)
    os.makedirs(config['Paths']['workdir'])
    os.makedirs(config['Paths']['projectdir'])
    os.makedirs(config['Paths']['datasetdir'])
 
