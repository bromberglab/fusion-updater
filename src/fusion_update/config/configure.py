"""
Module that contains the functions to create, import and vaildate the 
configuration file that is at the backbone of the whole fusion updater 
package.
"""

__author__ = 'Yannick Mahlich'
__email__ = 'ymahlich@bromberglab.org'

import os
import os.path
import sys
import configparser
from configparser import ExtendedInterpolation


def create_config_template(configfile):
    """
    Creates a template config file and saves it to the filehandle stored 
    in the 'configfile' argument.
    """

    config = configparser.ConfigParser(interpolation=ExtendedInterpolation())
    config['PATHS'] = {
        'WorkDir': '/the/main/workdirectory/goes/here',
        'ProjectDir': '/the/directory/where/to/store/final/datasets'
    }
    config['Proteomes'] = {
        'data_source' : 'refseq',
        'ftp_server' : 'https://ftp.ncbi.nlm.nih.gov',
        'assembly_info_path' : '${ftp_server}/genomes/${data_source}/bacteria/assembly_summary.txt',
        'assembly_level': 'Complete Genome'
    }
    config['Downloads'] = {
        'max_files_per_folder': 5000
    }
    config.write(configfile)

def import_config(configfile):
    """
    Imports a modified config file (the function argument is the
    filehandle) and stores it into src/fusion_update/config/config.ini. 
    The config file stored there is then used internally in other 
    functions to retrieve important configuration values such as the 
    path to the working directory or where results should be stored.
    """

    config = configparser.ConfigParser(interpolation=ExtendedInterpolation())
    config.read(configfile)
    config_fpath = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'config.ini'
        )
    with open(config_fpath, 'w') as configfile:
        config.write(configfile)

def read_config():
    """
    Helper function to read the stored configuration.
    """
    config = configparser.ConfigParser(interpolation=ExtendedInterpolation())
    config_fpath = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'config.ini'
        )
    config.read(config_fpath)
    return config

def save_config(config):
    config_fpath = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'config.ini'
        )
    with open(config_fpath, 'w') as configfile:
        config.write(configfile)

def validate_config():
    """
    Prints the configuration to stdout for user validation respective 
    confirmation that the configuration has been properly stored and is 
    ready to use.
    """
    config = read_config()
    config.write(sys.stdout)

